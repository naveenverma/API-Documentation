# API Documentation

## User Management System

1.  
    - API Name: User Registration API
    - URL: /v1/user/register/
    - Module: user
    - Method: POST
    - Header: Platform-Header
    - Request:

        ```json 
        {
    	    "user_name":"Sample@1",
    	    "email":"abcdefg@gmail.com",
    	    "password":"Qwerty@123"
        }
        ```
    
        - Response: __EXCEPTIONED_GENERIC_STATUS_RESPONSE_FORMAT__
            - __HTTP/1.1 200 OK__

        ```json
        {
            "meta": "",
            "data": {
                "profile": {
                    "user_name": "Sample@1",
                    "secret_key": "5RHAQBFQ447PUKY5",
                    "user_id": 4,
                    "email": "abcdefg@gmail.com"
                }
            }
        }
        ```
        
        - __HTTP/1.1 404 Not Found__: Use Generic Response
        
        - __HTTP/1.1 400 Bad Request__: 
            - If a user with that user_name already exists in the database or 
                it does not contain atleast one number or atleast one special char
                or atleast one upper case letter or atleast one lower case letter
                or contains whitespace at starting or in the end.

            ```json
            {
                "meta":"",
                "data":{
                    "errors": "The user name is either taken or does not fits in criteria"
                }
            }
            ```
            
            - If a password does not contain atleast one number or atleast one special char
                or atleast one upper case letter or atleast one lower case letter
                or contains whitespace at starting or in the end.

            ```json
            {
                "meta":"",
                "data":{
                    "errors": "The password does not fits in the criteria"
                }
            }
            ```

2. 
    - API Name: User Login API
    - URL: /v1/user/login/
    - Module: user
    - Method: POST
    - Header: Platform-Header
    - Request:

    ```json
    {
        "user_name":"Sample@1",
        "password":"Qwerty@123",
        "otp_key": "123456"
    }
    ```
    - Response: __EXCEPTIONED_GENERIC_STATUS_RESPONSE_FORMAT__
        - __HTTP/1.1 200 OK__ 

        ```json
        {
            "meta": "",
            "data": {
            
                "user_id": 4,
                "session_token": "User_4_KgPX4zMWS2gTWbptyPA1nIY5cVpfLf8S"
            }
        }
        ```
        - __HTTP/1.1 404 Not Found__: Use Generic Response
        - __HTTP/1.1 400 Bad Request__: 
            - If the user_name is not registered

            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "This User_name is not registred"
                    ]
                }
            }
            ```
            - If the otp_key is wrong

            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "The OTP is incorrect"
                    ]
                }
            }
            ```
            -If the password is wrong
            
            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "The password is incorrect"
                    ]
                }
            }
            ```
3. 
    - API Name: User Logout API
    - URL: /v1/user/logout/
    - Module: user
    - Method: POST
    - Header: Loggedin-Platform-Header
    - Request:
    
        ```json
        {
            "user_id": 4
        }
        ```
    - Response: __ALL_GENERIC_STATUS_RESPONSE_FORMAT__
        
        -HTTP 200_OK : 
        
        ```json
        {
            "meta": "",
            "data": {}
        }
        ```

4.  
    - API Name: Admin User Registration API
    - URL: /v1/user/register/
    - Module: admin_user
    - Method: POST
    - Header: Platform-Header
    - Request:

        ```json 
        {
    	    "admin_user_name":"Sample@1",
    	    "email":"abcdefg@gmail.com",
    	    "password":"Qwerty@123"
        }
        ```
    
        - Response: __EXCEPTIONED_GENERIC_STATUS_RESPONSE_FORMAT__
            - __HTTP/1.1 200 OK__

        ```json
        {
            "meta": "",
            "data": {
                "profile": {
                    "admin_user_name": "Sample@1",
                    "admin_user_id": 4,
                    "email": "abcdefg@gmail.com"
                }
            }
        }
        ```
        
        - __HTTP/1.1 404 Not Found__: Use Generic Response
        
        - __HTTP/1.1 400 Bad Request__: 
            - If a admin user with that admin_user_name already exists in the database or 
                it does not contain atleast one number or atleast one special char
                or atleast one upper case letter or atleast one lower case letter
                or contains whitespace at starting or in the end.

            ```json
            {
                "meta":"",
                "data":{
                    "errors": "The admin user name is either taken or does not fits in criteria"
                }
            }
            ```
            
            - If a password does not contain atleast one number or atleast one special char
                or atleast one upper case letter or atleast one lower case letter
                or contains whitespace at starting or in the end.

            ```json
            {
                "meta":"",
                "data":{
                    "errors": "The password does not fits in the criteria"
                }
            }
            ```

5. 
    - API Name: Admin User Login API
    - URL: /v1/admin_user/login/
    - Module: admin_user
    - Method: POST
    - Header: Platform-Header
    - Request:

    ```json
    {
        "admin_user_name":"Admin@123",
        "password":"Qwerty@123"
    }
    ```
    - Response: __EXCEPTIONED_GENERIC_STATUS_RESPONSE_FORMAT__
        - __HTTP/1.1 200 OK__ 

        ```json
        {
            "meta": "",
            "data": {
            
                "admin_user_id": 4,
                "session_token": "Admin_4_KgPX4zMWS2gTWbptyPA1nIY5cVpfLf8S"
            }
        }
        ```
        - __HTTP/1.1 404 Not Found__: Use Generic Response
        - __HTTP/1.1 400 Bad Request__: 
            - If the user_name is not registered

            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "This Admin_user_name is not registred"
                    ]
                }
            }
            ```
            -If the password is wrong
            
            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "The password is incorrect"
                    ]
                }
            }
            ```
6. 
    - API Name: AdminUser Logout API
    - URL: /v1/admin_user/logout/
    - Module: admin_user
    - Method: POST
    - Header: Loggedin-Platform-Header
    - Request:
    
        ```json
        {
            "admin_user_id": 4
        }
        ```
    - Response: __ALL_GENERIC_STATUS_RESPONSE_FORMAT__
        
        -HTTP 200_OK : 
        
        ```json
        {
            "meta": "",
            "data": {}
        }
        ```

7.
    - API Name: AdminUser Config API
    - URL: /v1/admin_user/config/
    - Module: admin_user
    - Method: GET
    - Response: __ALL_GENERIC_STATUS_RESPONSE_FORMAT__
        
        -HTTP 200_OK : 
        
        ```json
        {
            "meta": "",
            "data": {
                "config": {
                    "no_of_cows": 20,
                    "heads_probability": 0.5,
                    "Prime_minister": "NaModi"
                }
            }
        }
        ```